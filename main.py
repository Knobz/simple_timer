import time
import kivy
kivy.require('2.1.0')
from kivy.app import App
from kivy.app import Builder
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window
from kivy.animation import Animation
from gpiozero import Buzzer
Window.size = (800, 480)
Window.show_cursor = False
Window.fullscreen = True
MAX_TIME = 180

buzzer = Buzzer(3)

Builder.load_string("""
<timer>
    canvas.before:
        Color:
            rgba: 0, 0, 0, 1
        Rectangle:
            pos: self.pos
            size: self.size
<Label>
    background_color: 0,0,0,1
    canvas.before:
        Color:
            rgba: self.background_color
        Rectangle:
            pos: self.pos
            size: self.size

""")
        
class timer(FloatLayout):
    def __init__(self, **kwargs):
        super(FloatLayout, self).__init__(**kwargs)
        self.size=(800, 480)
        self.start_time = 0
        self.running = 0

        self.b= Button(text="START", background_color=(0,0,0,1), width=800, height=480,pos=(0,0), font_size="200px")
        self.b.bind(on_press=self.start_timer)
        self.add_widget(self.b)

    def start_timer(self,b):
        self.start_time = time.monotonic()
        self.running = Clock.schedule_interval(self.check_time, 0.1)
        self.b.disabled=True
        self.l= Label(text="START", color=(1,1,1,1), width=800, height=480,pos=(0,0), font_size="200px")
        self.add_widget(self.l)
        
    
    def check_time(self,bt):
        current_time = int(time.monotonic()-self.start_time)
        if current_time <= MAX_TIME:
            minutes = str((MAX_TIME-current_time) // 60)
            seconds = (MAX_TIME-current_time) % 60
            if seconds < 10:
                seconds = "0" + str(seconds)
            self.l.text= f"{minutes}:{seconds}"
        else:
            self.running.cancel()
            self.run_animation()
            
    def run_animation(self):
        anim  = Animation(background_color=(1,0,0,1), duration=0.3) + Animation(background_color=(0,0,0,1), duration=0.7)
        for i in range(4):
            anim  += Animation(background_color=(1,0,0,1), duration=0.3) + Animation(background_color=(0,0,0,1), duration=0.7)
        anim.start(self.l)
        buzzer.beep(on_time=0.7, off_time=0.3, n=5, background=True)
        anim.bind(on_complete=self.reset)
        
            
    def reset(self,*kwargs):
        self.b.disabled=False
        self.remove_widget(self.l)
    
class TimerApp(App):
    def build(self):
        return timer()


if __name__ == '__main__':
    TimerApp().run()